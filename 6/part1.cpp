#include <iostream>
#include <fstream>
#include <list>
using namespace std;

int main(void) {

    string str;
    fstream fs;
    list<char> clis;

    fs.open("./input", ios::in);
    getline(fs, str);
    fs.close();

    for (size_t i = 0; i < str.size()-1; i++) {
      clis.push_back(str.at(i));
      clis.push_back(str.at(i+1));
      clis.push_back(str.at(i+2));
      clis.push_back(str.at(i+3));
      clis.push_back(str.at(i+4));
      clis.push_back(str.at(i+5));
      clis.push_back(str.at(i+6));
      clis.push_back(str.at(i+7));
      clis.push_back(str.at(i+8));
      clis.push_back(str.at(i+9));
      clis.push_back(str.at(i+10));
      clis.push_back(str.at(i+11));
      clis.push_back(str.at(i+12)); // la parte 1 y 2 son lo mismo con difentes cantidades de comprobaciones (es sub-optimo)
      clis.push_back(str.at(i+13));
      clis.sort();
      clis.unique();
      if (clis.size() == 14) {
        cout << i+14 << " ";
        for (list<char>::iterator it = clis.begin(); it!=clis.end(); ++it){
          cout << *it;
        }
        break;
      } else {
        clis.clear();
      }
    }

    return 0;
}
