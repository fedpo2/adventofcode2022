#include <iostream>
#include <fstream>

using std::getline;
using std::string;

int main (void) {
  enum opcion_t {
    A=1, B=2, C=3
  } opcion;

  string str;
  int res = 0;
  char car[2];
  std::fstream fp;
  fp.open("input", std::ios::in);

  while (getline(fp, str)) { 
    //std::cout << str << std::endl;
    // A>C C>B B>A
    // Pi = A = X    Pa = B = Y   T = C = Z
    
    if (str != "") {
      if (str.back() == 'X') {
        if (str.front() == 'A') {
          res += 4;
        } else if (str.front() == 'C') {
          res += 7;
        } else {
          res++;
        }
      }else if (str.back() == 'Y') {
        if (str.front() == 'B') {
          res += 5;
        } else if (str.front() == 'A') {
          res += 8;
        } else {
          res += 2;
        }
      }else if (str.back() == 'Z' ){
        if (str.front() == 'C') {
          res += 6;
        } else if (str.front() == 'B') {
          res += 9;
        }else {
          res += 3;
        }
      }
    }
  }
  fp.close();
  std::cout << "puntos totales: " << res << std::endl;
  return 0;
}
