#include <iostream>
#include <fstream>
#include <vector>

using std::fstream;
using std::getline;
using std::string;
using std::vector;

void borrar_repetidos(string &str){
  for (size_t i = 0; i < str.size(); i++) {
    for (size_t j = 0; j < str.size(); j++) {
      if (str[i] == str[j] && i != j) {
        str.erase(str.begin()+j);
      }
    }
  }
}

int main (void) {
  int len;
  char tmp;

  vector<string> *vec = new vector<string>();
  vector<char> *charvec = new vector<char>();
  string str, base;
  fstream fp;

  fp.open("./input", std::ios::in);
  while (getline(fp, base)) {
    if (base != "") {
      vec->push_back(base);
    }
  }
  fp.close();

  for (int i = 0; i < vec->size(); i++) {
    str = vec->at(i);
    base.assign(str, 0, str.size()/2);
    str.assign(str, str.size()/2, str.size()/2);
    
     borrar_repetidos(str);
     borrar_repetidos(base);
    
    for (size_t i = 0; i < str.size(); i++) {
      for (size_t j = 0; j < base.size(); j++) {
        if (str[i] == base[j]) {
          charvec->push_back(str[i]);
          break;
        }
      }
    }
  }
  delete vec;
  len = 0;
  for (size_t i = 0; i < charvec->size(); i++) {
    tmp = charvec->at(i);
    if (tmp >= 'A' && tmp <= 'Z') {
      len+=int(tmp)-'A'+27;
    } else if(tmp >= 'a' && tmp <= 'z') {
      len+=int(tmp)-'a'+1;
    }
    std::cout << len << " " << int(tmp) << " " << charvec->at(i) << std::endl;
  }
  std::cout << len << std::endl;
/*
  fp.open("./out", std::ios::app);
  for (int i = 0; i < vec.size(); i++) {
    fp << vec[i] << std::endl;
  }
  fp.close();
*/


  return 0;
}
