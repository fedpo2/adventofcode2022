#include <iostream>
#include <fstream>
#include <vector>

using std::fstream;
using std::getline;
using std::string;
using std::vector;

char triple_igualdad(string &str, string &str2, string &str3) {
  for (int j = 0; j < str.size(); j++) {
    for (int h = 0; h < str2.size(); h++) {
      for (int k = 0; k < str3.size(); k++) {
        if (str[j] == str2[h] && str[j] == str3[k] && str2[h] == str3[k]) {
          return str[j];
        } 
      }
    }
  }
}

void borrar_repetidos(string &str){
  for (size_t i = 0; i < str.size(); i++) {
    for (size_t j = 0; j < str.size(); j++) {
      if (str[i] == str[j] && i != j) {
        str.erase(str.begin()+j);
      }
    }
  }
}

int main (void) {
  int len;
  char tmp;

  vector<string> *vec = new vector<string>();
  vector<char> *charvec = new vector<char>();
  string str, str2, str3;
  fstream fp;

  fp.open("./input", std::ios::in);
  while (getline(fp, str)) {
    if (str != "") {
      vec->push_back(str);
    }
  }
  fp.close();

  for (int i = 0; i < vec->size(); i+=3) {
    
    str = vec->at(i);
    str2 = vec->at(i+1);
    str3 = vec->at(i+2);

    borrar_repetidos(str);
    borrar_repetidos(str2);
    borrar_repetidos(str3);

    charvec->push_back(triple_igualdad(str, str2, str3));
  }

  delete vec;
  len = 0;
  for (size_t i = 0; i < charvec->size(); i++) {
    tmp = charvec->at(i);
    if (tmp >= 'A' && tmp <= 'Z') {
      len+=int(tmp)-'A'+27;
    } else if(tmp >= 'a' && tmp <= 'z') {
      len+=int(tmp)-'a'+1;
    }
    std::cout << len << " " << int(tmp) << " " << charvec->at(i) << std::endl;
  }
  delete charvec;
    std::cout << len << std::endl;
  
  return 0;
}
