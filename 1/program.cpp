#include <iostream>
#include <fstream>

using std::string;
using std::getline;

int main (void) {
  int a, b;
  string str;
  
  std::fstream fp;
  fp.open("./input", std::ios::in);

  while (getline(fp, str)) {
    
    if (str == "") {
      if (b<a) {
        b = a;
      }
      a = 0;
    } else {
      a+=std::stoi(str);
    }
  }

  std::cout << b << std::endl;

  return 0;
}
