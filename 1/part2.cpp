#include <iostream>
#include <algorithm>
#include <fstream>
#include <vector>

using std::string;
using std::getline;
using std::vector;

bool sortingfunction(int i, int j){
  return (i<j);
}
int main (void) {
  int tmp = 0, total = 0; 
  vector<int> nums;
  string str;
  
  std::fstream fp;
  fp.open("./input", std::ios::in);

  while (getline(fp, str)) {
    
    if (str == "") {
      nums.push_back(tmp);
      tmp = 0;
    } else {
      tmp += std::stoi(str);
    }
  }
  fp.close(); 
  
  std::sort(nums.begin(), nums.end(), sortingfunction);

  for (unsigned i = 0; i < 3; i++) {
     total += nums.back();
     std::cout << "top "<< i+1 << ": " << nums.back() << std::endl;
     nums.pop_back();
  }
  std::cout << "total: " << total << std::endl;
  return 0;
}
