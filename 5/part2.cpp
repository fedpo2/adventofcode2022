#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <cstdio>

using std::string;

void do_movements(std::vector<string>* svec, std::vector<string>* resp){
    int tmp[3];
    string str;
    for (size_t i = 8; i < svec->size()-1; i++) {
        str = "";
        std::stringstream X(svec->at(i));

        std::getline(X,str,' ');
        tmp[0] = std::stoi(str);

        std::getline(X,str,' ');
        tmp[1] = std::stoi(str);

        std::getline(X,str,' ');
        tmp[2] = std::stoi(str);


        str.assign(resp->at(tmp[1]-1).end()-tmp[0], resp->at(tmp[1]-1).end());

//        std::cout << tmp[0] << tmp[1] << tmp[2] << str << "\n";

        resp->at(tmp[1]-1).erase(resp->at(tmp[1]-1).end()-tmp[0], resp->at(tmp[1]-1).end());
        resp->at(tmp[2]-1).append(str);

    }
}

void string_loader(std::vector<string>* svec, std::vector<string>* resp){
	int j = 0;
	for (size_t tmp = 0; tmp < 18; tmp+=2) {
		for (size_t i = 7; i > 0; i--) {
			if (svec->at(i)[tmp] != ' ') {
				resp->at(j).push_back(svec->at(i)[tmp]);

			}
		}
		j++;
	}
	j = 0; //HACK: no se hacer un for reverso así que hardcodié la pasada que no me estaba haciendo el for, deberia de ser 1 instruccion más lento que hacerlo bien
	for (size_t tmp = 0; tmp < svec->at(0).size(); tmp+=2) {
		if (svec->at(0)[tmp] != ' ') {
			resp->at(j).push_back(svec->at(0)[tmp]);
		}
		j++;
	}
}

void format_inputs(string& str){
	str.erase(0, 5);
	str.erase(str.find(" fr"), 5);
	str.erase(str.find(" to"), 3);
}

void remove_chars(string& str){
	for(size_t i = 0; i < str.size(); i++){ // NOTE: Esto no falla porque un str termina en \0, en otros casos un vector en null //std::out_of_range
		if(str[i] < 65 || str[i] > 90){
			str.erase(i, 1);
		}
	}
}

int main (void){
	std::fstream fs; //FILE* f; fopen(f, ".in", "i");
	std::vector<string> *svec = new std::vector<string>();
	std::vector<string> *resp = new std::vector<string>();

	string *str = new string();
	fs.open("input", std::ios::in);
	while (std::getline(fs, *str)){
		svec->push_back(*str);
	}
	fs.close();
	delete str;

	for (int i = 0; i < 8; i++){
		remove_chars(svec->at(i));
	}
	svec->erase(svec->begin()+8);
	svec->erase(svec->begin()+8);
	for (size_t i = 0; i < 10; i++) resp->push_back("");

	for (size_t i = 8; i < svec->size()-1; i++) {
		format_inputs(svec->at(i));
	}
	string_loader(svec, resp);
	//NOTE: esto es un dump a consola de los datos del primer vector
	for (int i = 0; i < resp->size(); i++){
		std::cout << resp->at(i) << std::endl;
	}

	do_movements(svec, resp);

	for (size_t i = 0 ; i < resp->size()-1; i++ ) {
		std::cout << resp->at(i).back();
	}

	delete svec;
	delete resp;
	return 0;
}
