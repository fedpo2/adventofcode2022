#include <iostream>
#include <fstream>
#include <vector>

using std::stoi;
using std::string;
using std::fstream;
using std::vector;

void obtener_numeros(string &base, int *res, size_t *end){
  for (size_t i = 0; i < 4; i++) {
    *end = 0;
    res[i] = stoi(base, end);
    if (*end+1 < base.size()) {
    base = base.substr(*end+1);
    }
  }
}

int main (void)
{
  vector<string> *strvec = new vector<string>();
  int comparar[4]; 
  fstream fs;
  
  string *str = new string();
  fs.open("./in", std::ios::in);
  while (getline(fs, *str)) {
    strvec->push_back(*str);
  } 
  fs.close();
  delete str;

  size_t *end = new size_t();
  int resultado = 0;
  for (size_t i = 0; i < strvec->size(); i++) {
    obtener_numeros(strvec->at(i), comparar, end);
    
    if (comparar[0] <= comparar[2] && comparar[1] >= comparar[3] || comparar[0] >= comparar[2] && comparar[1] <= comparar[3]) {
      resultado++;
      std::cout << resultado <<" "<< comparar[0] <<" "<< comparar[1] <<" "<< comparar[2] <<" "<< comparar[3] <<std::endl;
    }
  }
  std::cout << resultado << std::endl;
  delete end;
  delete strvec;
  return 0;
}


